package ru.ustits;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;

import static org.junit.Assert.*;

/**
 * @author ustits
 */
public class ClassBTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  private ClassB classB = new ClassB();

  @Test
  public void testWrite() throws Exception {
    File file = folder.newFile();
    classB.writeToFile("test", file.getPath());
    byte[] bytes = Files.readAllBytes(file.toPath());
    assertEquals("test", new String(bytes));
  }
}