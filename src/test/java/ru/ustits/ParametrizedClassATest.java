package ru.ustits;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * @author ustits
 */
@RunWith(Parameterized.class)
public class ParametrizedClassATest {

  @Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
            { 0, 0, 0 },
            { 1, 3, 4 },
            { 39, 3, 42 }
    });
  }

  private int a;
  private int b;
  private int expected;

  private ClassA classA = new ClassA();

  public ParametrizedClassATest(int a, int b, int expected) {
    this.a = a;
    this.b = b;
    this.expected = expected;
  }

  @Test
  public void test() {
    assertEquals(expected, classA.sum(a , b));
  }
}
