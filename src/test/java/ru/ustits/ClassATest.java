package ru.ustits;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author ustits
 */
public class ClassATest {

  private ClassA classA;

  @Before
  public void setUp() throws Exception {
    classA = new ClassA();
  }

  @Test
  public void testDecorate() throws Exception {
    String result = classA.decorate("test");
    assertTrue(result.contains("test"));
  }

  @Test(expected = NullPointerException.class)
  public void testThrowNPE() throws Exception {
    classA.decorate(null);
  }

  @Ignore
  @Test
  public void testBrokenTest() throws Exception {
    throw new RuntimeException();
  }
}