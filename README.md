# Мигрируем с JUnit4 на JUnit5

[Хорошая вводная по фичам junit5](http://www.baeldung.com/junit-5-migration)

## Структура:
![structure](/structure.jpg)

[Сорц картинки](https://twitter.com/niksw7/status/892994598053662720)

## Примечания из документации
> The entire framework was contained in a single jar library. The whole library needs to be imported even when only a particular feature is required. In JUnit 5, we get more granularity and can import only what is necessary

> One test runner can only execute tests in JUnit 4 at a time (e.g. SpringJUnit4ClassRunner or Parameterized ). JUnit 5 allows multiple runners to work simultaneously

> JUnit 4 never advanced beyond Java 7, missing out on a lot of features from Java 8. JUnit 5 makes good use of Java 8 features
JUnit не поддерживается с Dec 04, 2014

## Как запускать на junit4

* [Как запускать с junit4](http://junit.org/junit5/docs/current/user-guide/#running-tests-build-maven)
* [Еще один пример](https://github.com/junit-team/junit5-samples/blob/master/junit5-maven-consumer/pom.xml)

### Коротко
Надо в зависимости добавить
* `jupiter-api` и `jupiter-engine` - площадки для тестов
* `surefire` - чтобы гонялись тесты в idea
* `vintage` - чтобы гонялись junit4 тесты

## Что поменяли

1. В классах и методах не надо писать public
2. Поменяли тестовые аннотации:
    * `@BeforeAll` - `@BeforeClass`
    * `@BeforeEach` - `@Before`
    * `@AfterAll` - `@AfterClass`
    * `@AfterEach` - `@After`
    * `@Disabled` - `@Ignore` 
3. `Assertion` и `Assumptions` пополнели и работают с лямбдами
4. `@RepeatedTest` - повторяющиеся тесты
5. `@ExtendWith` - замена `@RunWith` и `@Rule`. Главные фишки:
   * можно делать несколько
   * можно делать только для одного метода
   * [примеры](/src/test/java/ru/ustits/extensions)
6. `@ParametrizedTest` - **экспериментальная**
    * требует дополнительной зависимости `junit-jupiter-params`
    * [пример](/src/test/java/ru/ustits/ParametrizedClassA5Test.java)
    * [больше примеров](http://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests)
7. `@Nested` - можно писать тесты во вложенных классах [ссылка](http://junit.org/junit5/docs/current/user-guide/#writing-tests-nested)
8. Для ловли exception больше нету `@Test(expected=...)`, вместо него предлагают использовать такие конструкции:
```
assertThrows(NullPointerException.class,
            () -> classA.decorate(null));
```
Собственно не проблема, потому что `assertJ`, например, проповедует такую же идеалогию.
